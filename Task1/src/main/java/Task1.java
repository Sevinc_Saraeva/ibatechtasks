import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Hello, what is your name?");
        String name = in.nextLine();
        System.out.println("Hello, " + name  + " !");
        System.out.println("Nice to meet you,  " + name  + " !");
        System.out.println("how old are you?");
        int age = in.nextInt();
        if(age<18) System.out.println("Let's go to cinema");
        else System.out.println("Let's go to nightclub!");
        System.out.println("Bye! ");



    }
}
