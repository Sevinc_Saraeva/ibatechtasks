import java.util.Random;
import java.util.Scanner;

public class Lesson4 {
    public static void main(String[] args) {
        Random random = new Random();
        int charNumber = random.nextInt(90-65+1)+65;
        //System.out.println(lower_case);

      //  System.out.println((char)(lower_case));


        System.out.println("lowercase " + getLowerCaseString());
        System.out.println("uppercase " + getUpperCaseString());
        System.out.println("vowelSmallString " + vowelSmallString());
        System.out.println("consonantSmallString " + consonantSmallString());

        System.out.println( " vowelCapitalString " + vowelCapitalString());
        System.out.println(" constantCapitalString() " + constantCapitalString());
        System.out.println(" mixedString() " + mixedString()) ;
        System.out.println(" lowerLetter " + lowerLetter("vfdkvfVDFGBGFBGB"));

        System.out.println(" upperLetter " + upperLetter("vfdkvfVDFGBGFBGB"));

        System.out.println(" vowelLetter " + lowerLetter("vfdkvfVedeuuaaaDFGBGFBGB"));

        System.out.println(" constantLetter " + upperLetter("vaaaafdkuuuuvfVDFGBGFBGB"));


        System.out.println(" constant capital Letter " + constantCapitalLetter("vfdkeeaaiuuAAAEEEUvfVDFGBGFBGB"));

        System.out.println(" Capital vowel Letter " + vowelCapitalLetter("vAAAUUUaaaauuufdkvfVDFGBGFBGB"));

        System.out.println(" constant small Letter " + constantSmallLetter("vfdkvfAAAVedeuuaaaDFGBGFBGB"));

        System.out.println(" vowel Small Letter " + vowelSmallLetter("vaaaafdkuuAAAUUUuuvfVDFGBGFBGB"));

        System.out.println(" constant  Letter " + constantLetter("vfdkvfAAAVedeuuaaaDFGBGFBGB"));

        System.out.println(" vowel  Letter " + vowelLetter("vaaaafdkuuAAAUUUuuvfVDFGBGFBGB"));


       Scanner in = new Scanner(System.in);
       String text = in.nextLine();




    }
 public static StringBuilder getLowerCaseString(){
     Random random = new Random();
     StringBuilder lowercase_string = new StringBuilder();
     for (int i = 0; i <30; i++) {
         int charNumber = random.nextInt(122-97+1)+97;
         lowercase_string.append((char)(charNumber));
     }
     return lowercase_string;
 }


    public static StringBuilder getUpperCaseString(){
        Random random = new Random();
        StringBuilder uppercase_string = new StringBuilder();
        for (int i = 0; i <30; i++) {
          int  charNumber = random.nextInt(90-65+1)+65;
            uppercase_string.append((char)(charNumber));
        }
        return uppercase_string;
    }

    public static StringBuilder vowelSmallString(){
        Random random = new Random();
        StringBuilder vowelSmallString = new StringBuilder();
        while (vowelSmallString.length() != 20 ) {
           int charNumber = random.nextInt(122-97+1)+97;
            if(charNumber == 97 || charNumber==101
                    || charNumber==105 || charNumber==111 || charNumber==117) {
                if (vowelSmallString.length() != 20)
                    vowelSmallString.append((char) (charNumber));
            }

        }
        return vowelSmallString;

    }


    public static StringBuilder consonantSmallString(){
        StringBuilder consonantSmallString = new StringBuilder();
        Random random = new Random();
        while (consonantSmallString.length() != 20 ) {
            int charNumber = random.nextInt(122-97+1)+97;
            if(charNumber != 97 || charNumber!=101
                    || charNumber!=105 || charNumber!=111 || charNumber!=117) {
                if (consonantSmallString.length() != 20)
                    consonantSmallString.append((char) (charNumber));
            }

        }
        return consonantSmallString;
    }




    public  static StringBuilder vowelCapitalString(){
        Random random = new Random();
        StringBuilder vowelCapitalString = new StringBuilder();
        while(vowelCapitalString.length()!=20){
           int charNumber = random.nextInt(90-65+1)+65;
            if(charNumber == 65 || charNumber==69
                    || charNumber==73 || charNumber==79 || charNumber==85){
                if(vowelCapitalString.length()!=20)
                    vowelCapitalString.append((char)(charNumber));}

        }
             return vowelCapitalString;
    }
    public  static StringBuilder constantCapitalString(){
        Random random = new Random();
        StringBuilder constantCapitalString = new StringBuilder();
        while(constantCapitalString.length()!=20){
            int charNumber = random.nextInt(90-65+1)+65;
            if(charNumber != 65 || charNumber!=69
                    || charNumber!=73 || charNumber!=79 || charNumber!=85){
                if(constantCapitalString.length()!=20)
                    constantCapitalString.append((char)(charNumber));}

        }
        return constantCapitalString;
    }
    public static StringBuilder mixedString(){
        Random random = new Random();
        StringBuilder mixedString = new StringBuilder();
        while(mixedString.length()!=35) {
           int charNumber = random.nextInt(122-65+1)+65;
            if(charNumber<91 || charNumber>96){
                mixedString.append((char)(charNumber));
            }
        }
        return mixedString;
    }




public static StringBuilder lowerLetter(String text){
        StringBuilder lowerLetter = new StringBuilder();
    for (int i = 0; i < text.length(); i++) {
        if(text.charAt(i)>=97 && text.charAt(i)<=122)
            lowerLetter.append(text.charAt(i));

    }
    return lowerLetter;
}
    public static StringBuilder upperLetter(String text){
        StringBuilder upperLetter = new StringBuilder();
        for (int i = 0; i < text.length(); i++) {
            if(text.charAt(i)<97 || text.charAt(i)>122)
                upperLetter.append(text.charAt(i));

        }
        return upperLetter;
    }
    public static StringBuilder vowelLetter(String text){
        StringBuilder vowelLetter = new StringBuilder();
        for (int i = 0; i < text.length(); i++) {
            if(text.charAt(i)==65 || text.charAt(i)==69 || text.charAt(i)==73
                    || text.charAt(i)==79 || text.charAt(i)==85 ||
            text.charAt(i)==97 || text.charAt(i)==101 || text.charAt(i)==105
                    || text.charAt(i)==111 || text.charAt(i)==117 )
                vowelLetter.append(text.charAt(i));

        }
return  vowelLetter;
    }


    public static StringBuilder constantLetter(String text){
        StringBuilder constantLetter = new StringBuilder();
        for (int i = 0; i < text.length(); i++) {
            if(text.charAt(i)!=65 || text.charAt(i)!=69 || text.charAt(i)!=73
                    || text.charAt(i)!=79 || text.charAt(i)!=85 ||
                    text.charAt(i)!=97 || text.charAt(i)!=101 || text.charAt(i)!=105
                    || text.charAt(i)!=111 || text.charAt(i)!=117 )
                constantLetter.append(text.charAt(i));

        }
        return  constantLetter;
    }


    public static StringBuilder vowelCapitalLetter(String text){

        StringBuilder vowelCapitalLetter = new StringBuilder();
        for (int i = 0; i < text.length(); i++) {
            if(text.charAt(i)>=65 && text.charAt(i)>=90)
            if(text.charAt(i)==65 || text.charAt(i)==69 || text.charAt(i)==73
                    || text.charAt(i)==79 || text.charAt(i)==85 )
                vowelCapitalLetter.append(text.charAt(i));

        }
        return  vowelCapitalLetter;


    }

    public static StringBuilder constantCapitalLetter(String text){
        StringBuilder constantCapitalLetter = new StringBuilder();
        for (int i = 0; i < text.length(); i++) {
            if(text.charAt(i)>=65 && text.charAt(i)<=90)
            if(text.charAt(i)!=65 || text.charAt(i)!=69 || text.charAt(i)!=73
                    || text.charAt(i)!=79 || text.charAt(i)!=85 )
                constantCapitalLetter.append(text.charAt(i));

        }
        return  constantCapitalLetter;
    }

    public static StringBuilder vowelSmallLetter(String text){
        StringBuilder vowelSmallLetter = new StringBuilder();
        for (int i = 0; i < text.length(); i++) {
            if(text.charAt(i)>=97 && text.charAt(i)<=122)
            if(text.charAt(i)==97 || text.charAt(i)==101 || text.charAt(i)==105
                    || text.charAt(i)==111 || text.charAt(i)==117 )
                vowelSmallLetter.append(text.charAt(i));

        }
        return  vowelSmallLetter;
    }

    public static StringBuilder constantSmallLetter(String text){
        StringBuilder constantSmallLetter = new StringBuilder();
        for (int i = 0; i < text.length(); i++) {
            if(text.charAt(i)>=65 && text.charAt(i)<=90)
                if(text.charAt(i)!=65 || text.charAt(i)!=69 || text.charAt(i)!=73
                        || text.charAt(i)!=79 || text.charAt(i)!=85 )
                    constantSmallLetter.append(text.charAt(i));

        }
        return  constantSmallLetter;
    }
}
