import java.util.Scanner;

public class Lesson3 {
    public static void main(String[] args) {
     Scanner in = new Scanner(System.in) ;
    int width = in.nextInt();
    int height = in.nextInt();
        for (int i = 0; i <height; i++) {
            if (i == 0 || i == height - 1) {
                for (int j = 0; j < width; j++) {
                    System.out.print("*");
                }
                System.out.println();

            } else {
                int count = 1;
                for (int j = 0; j < width; j++) {
                    if(j==0 || j==width-1 || j == i*3 || j==width-i*3) System.out.print("*");

                    else System.out.print(" ");


                }
                System.out.println();

            }
        }
    }
}
